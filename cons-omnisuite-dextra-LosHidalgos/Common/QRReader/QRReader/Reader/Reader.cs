﻿using Newtonsoft.Json;
using System.Xml.Serialization;
using QRReader.Enum;
using QRReader.Objects.ParseResult;
using System;

namespace QRReader
{
	public class Reader
	{
		public static QRCodeParseResult ParseQRCode(string QRCode)
		{
			if (QRCode.StartsWith("<mobiledevice>"))
			{
				return ParseXML(QRCode);
			}
			else
			{
				return ParseJSON(QRCode);
			}
		}

		static QRCodeParseResult ParseXML(string QRCode)
		{
			try
			{
				System.Xml.XmlReader reader = System.Xml.XmlReader.Create(new System.IO.StringReader(QRCode));

				XmlSerializer serializerXml = new XmlSerializer(typeof(LoyaltyContactParseResult));
				object o = serializerXml.Deserialize(reader);
				LoyaltyContactParseResult qrCodeData = (LoyaltyContactParseResult)o;

				return qrCodeData;
			}
			catch (Exception)
			{
				return new UnkownParseResult();
			}
		}

		static QRCodeParseResult ParseJSON(string QRCode)
		{
			try
			{

				var code = JsonConvert.DeserializeObject<QRCodeParseResult>(QRCode);
				switch (code.Function)
				{
					case QRCodeFunctions.MultipleBarcodes:
						return JsonConvert.DeserializeObject<MultipleBarcodesParseResult>(QRCode);
					case QRCodeFunctions.AddLoyaltyContact:
						return JsonConvert.DeserializeObject<LoyaltyContactParseResult>(QRCode);
					default:
						return JsonConvert.DeserializeObject<UnkownParseResult>(QRCode);
				}
			}
			catch (JsonException)
			{
				return new UnkownParseResult();
			}
			catch (Exception)
			{
				return new UnkownParseResult();
			}
		}

	}
}