﻿using System;
using System.Collections.Generic;

namespace QRReader.Objects.ParseResult
{
	public class MultipleBarcodesParseResult : QRCodeParseResult
	{
		public List<string> barcodes;
	}
}
