﻿using System;
using QRReader.Enum;

namespace QRReader.Objects.ParseResult
{
	public class QRCodeParseResult
	{
		public string function;
		public QRCodeFunctions Function
		{
			get
			{
				switch (this.function)
				{
					case "mb":
						return QRCodeFunctions.MultipleBarcodes;
					case "ac":
						return QRCodeFunctions.AddLoyaltyContact;
					default:
						return QRCodeFunctions.Unkown;
				};
			}
		}
	}
}
