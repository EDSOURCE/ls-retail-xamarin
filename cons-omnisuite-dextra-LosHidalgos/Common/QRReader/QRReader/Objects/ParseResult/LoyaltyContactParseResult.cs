﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace QRReader.Objects.ParseResult
{
	[XmlRoot("mobiledevice")]
	public class LoyaltyContactParseResult : QRCodeParseResult
	{
		[XmlElement("contactid")]
		public string ContactId { get; set; }

		[XmlElement("accountid")]
		public string AccountId { get; set; }

		[XmlElement("cardid")]
		public string CardId { get; set; }

		[XmlArray("coupons"), XmlArrayItem("cid")]
		public List<string> Coupons { get; set; }

		[XmlArray("offers"), XmlArrayItem("oid")]
		public List<string> Offers { get; set; }
	}
}
