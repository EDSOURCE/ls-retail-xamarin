﻿using System;
using Xunit;
using System.Resources;
using System.Reflection;
using QRReader;
using QRReader.Objects.ParseResult;

namespace TestQRCodeReader
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("QR CODER READER TESTER");

			int success = 0;

			success = ThisShouldReturnUnkownResult() ? ++success : success;
			success = ThisShouldReturnMultipleBarcodeResults() ? ++success : success;
			success = ConfirmNumberOfBarcodesEqualsFive() ? ++success : success;
			success = ThisShouldReturnLoyaltyContact() ? ++success : success;
			success = ThisShouldReturnUnkownResultBecauseOfBrokenXML() ? ++success : success;
			success = ThisShouldReturnUnkownResultBecauseOfBrokenJSON() ? ++success : success;

			Console.WriteLine(success + " of 6 tests passed");
		}

		public static bool ThisShouldReturnUnkownResult()
		{
			var unkown = Reader.ParseQRCode("Fuuu");

			try
			{

				Assert.IsType<UnkownParseResult>(
					unkown
				);
				Console.WriteLine("ThisShouldReturnUnkownResult success");

				return true;
			}
			catch
			{
				Console.WriteLine("ThisShouldReturnUnkownResult failed");

				return false;
			}
		}

		public static bool ThisShouldReturnMultipleBarcodeResults()
		{
			var unkown = Reader.ParseQRCode(TestResource.MultipleBarcode);

			try
			{
				Assert.IsType<MultipleBarcodesParseResult>(
					unkown
				);
				Console.WriteLine("ThisShouldReturnMultipleBarcodeResults success");

				return true;
			}
			catch
			{
				Console.WriteLine("ThisShouldReturnMultipleBarcodeResults failed");

				return false;
			}
		}

		public static bool ConfirmNumberOfBarcodesEqualsFive()
		{
			var unkown = Reader.ParseQRCode(TestResource.MultipleBarcode);

			try
			{
                Assert.IsType<MultipleBarcodesParseResult>(
					unkown
				);
				MultipleBarcodesParseResult barcodes = (MultipleBarcodesParseResult)unkown;
				bool barcodeCount = barcodes.barcodes.Count == 5;
				try
				{
					Assert.True(barcodeCount);
					Console.WriteLine("Correct number of barcodes");

					return true;
				}
				catch
				{
					Console.WriteLine("Wrong number of barcodes");

					return false;
				}
			}
			catch
			{
				Console.WriteLine("ConfirmNumberOfBarcodesEqualsFive failed");

				return false;
			}
		}

		public static bool ThisShouldReturnLoyaltyContact()
		{
			var unkown = Reader.ParseQRCode(TestResource.LoyaltyContact);

			try
			{
				Assert.IsType<LoyaltyContactParseResult>(
					unkown
				);
				Console.WriteLine("ThisShouldReturnLoyaltyContact success");

				return true;
			}
			catch
			{
				Console.WriteLine("ThisShouldReturnLoyaltyContact failed");

				return false;
			}
		}

		public static bool ThisShouldReturnUnkownResultBecauseOfBrokenXML()
		{
			var unkown = Reader.ParseQRCode(TestResource.BrokenXML);

			try
			{
				Assert.IsType<UnkownParseResult>(
					unkown
				);
				Console.WriteLine("ThisShouldReturnUnkownResultBecauseOfBrokenXML success");

				return true;
			}
			catch
			{
				Console.WriteLine("ThisShouldReturnUnkownResultBecauseOfBrokenXML failed");

				return false;
			}
		}

		public static bool ThisShouldReturnUnkownResultBecauseOfBrokenJSON()
		{
			var unkown = Reader.ParseQRCode(TestResource.BrokenJSON);

			try
			{
				Assert.IsType<UnkownParseResult>(
					unkown
				);
				Console.WriteLine("ThisShouldReturnUnkownResultBecauseOfBrokenJSON success");

				return true;
			}
			catch
			{
				Console.WriteLine("ThisShouldReturnUnkownResultBecauseOfBrokenJSON failed");

				return false;
			}
		}
	}
}
