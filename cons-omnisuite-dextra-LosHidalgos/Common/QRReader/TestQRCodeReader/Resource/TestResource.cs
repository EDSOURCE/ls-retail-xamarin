﻿using System;
using System.Collections.Generic;

namespace TestQRCodeReader
{
	public static class TestResource
	{
		public static string MultipleBarcode = 
			"{\n" +
			"\"function\": \"multiple_barcodes\",\n" +
			"\"barcodes\": [\"0123456789123\", \"0123456789123\", \"0123456789123\", \"0123456789123\", \"0123456789123\"]\n" +
			"}";

		public static string LoyaltyContact = 
			"<mobiledevice>" +
				"<contactid>{0}</contactid>" +
				"<accountid>{1}</accountid>" +
				"<cardid>{2}</cardid>" +
				"<coupons>" +
					"<cid>{0}</cid>" +
				"</coupons>" +
					"<offers>" +
						"<oid>{0}</oid>" +
				"</offers>" +
			"</mobiledevice>";
		public static string BrokenXML =
			"<mobiledevice>" +
				"<contactid>{0}</contactid>" +
				"<accountid>{1}</accountid>" +
				"<cardid>{2}</cardid>" +
				"<coupons>" +
					"<cid>{0}</cid>" +
				"</coupons>" +
					"<offers>" +
						"<oid>{0}</oid>" +
				"</offers>" +
			"</mobiledevice";
		public static string BrokenJSON =
			"{\n" +
			"\"function\": \"multiple_barcodes\",\n" +
			"\"barcodes\": [\"0123456789123\", \"0123456789123\", \"0123456789123\", \"0123456789123\", \"0123456789123\"]\n" +
			"";
	}
}
