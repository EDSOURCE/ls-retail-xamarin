using LSRetail.Omni.Domain.DataModel.Base.Menu;

namespace LSRetail.Omni.Domain.Services.Loyalty.Hospitality.Menus
{
	public interface IMenuRepository
	{
        MobileMenu GetMobileMenu();
	}
}