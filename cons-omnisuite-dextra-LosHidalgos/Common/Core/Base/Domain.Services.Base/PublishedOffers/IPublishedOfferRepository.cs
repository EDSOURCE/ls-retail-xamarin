﻿using System.Collections.Generic;
using LSRetail.Omni.Domain.DataModel.Base.Retail;

namespace LSRetail.Omni.Domain.Services.Base.PublishedOffers
{
	public interface IPublishedOfferRepository
	{
	    List<PublishedOffer> GetPublishedOffers(string itemId, string cardId, string storeId);
    }
}

