﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LSRetail.Omni.Domain.DataModel.Base.Retail;

namespace LSRetail.Omni.Domain.Services.Base.PublishedOffers
{
	public class PublishedOfferService
    {
	    IPublishedOfferRepository repo;

		public PublishedOfferService(IPublishedOfferRepository repo)
		{
			this.repo = repo;
		}

        public List<PublishedOffer> GetPublishedOffersByCardId(string cardId)
        {
            return repo.GetPublishedOffers(cardId, string.Empty, string.Empty);
        }

        public List<PublishedOffer> GetPublishedOffersByItemId(string itemId, string cardId)
        {
            return repo.GetPublishedOffers(cardId, itemId, string.Empty);
        }

        #region Async Functions

        public async Task<List<PublishedOffer>> GetPublishedOffersByCardIdAsync(string cardId)
        {
            return await Task.Run(() => GetPublishedOffersByCardId(cardId));
        }

        public async Task<List<PublishedOffer>> GetPublishedOffersByItemIdAsync(string itemId, string cardId)
        {
            return await Task.Run(() => GetPublishedOffersByItemId(itemId, cardId));
        }

        #endregion
    }
}

