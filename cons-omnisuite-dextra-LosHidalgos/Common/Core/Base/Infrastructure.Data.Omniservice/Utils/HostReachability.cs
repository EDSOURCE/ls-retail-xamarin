﻿using System;
using System.Threading;
using System.Threading.Tasks;
using LSRetail.Omni.Domain.DataModel.Base;

namespace LSRetail.Omni.Infrastructure.Data.Omniservice.Utils
{
	public static class HostReachability
	{
		public static CancellationTokenSource CancellationToken { get; set; }
        public static string HostName = Utils.DefaultUrlLoyalty;

		public delegate void OnHostStatusChangeEventHandler(bool online);
		public static event OnHostStatusChangeEventHandler HostChange;

		public static void Start()
		{
			Task.Run(() => Timer());
		}

		private static async Task Timer()
		{
			CancellationToken = new CancellationTokenSource();
			while (!CancellationToken.IsCancellationRequested)
			{
				try
				{
					//await Task.Delay(60000 - (int)(watch.ElapsedMilliseconds%1000), token);
					CancellationToken.Token.ThrowIfCancellationRequested();
					await Task.Delay(5000, CancellationToken.Token).ContinueWith((arg) =>
					{

						if (!CancellationToken.Token.IsCancellationRequested)
						{
							CancellationToken.Token.ThrowIfCancellationRequested();
							try
							{
								string status = Ping();
								if (HostChange != null)
								{
									HostChange(status.Length > 0);
								}
							}
							catch (LSOmniException)
							{
								if (HostChange != null)
								{
									HostChange(false);

								}
							}
						}
					});
				}
				catch (Exception ex)
				{
					System.Diagnostics.Debug.WriteLine("HostReachability.Timer Ex: " + ex.Message);
					//HostChange(false);
				}
			}

		}

		public static void Stop()
		{
			// Handle when your app sleeps
			if (CancellationToken != null)
			{
				CancellationToken.Cancel();
			}
		}

		private static string Ping()
		{
            Utils client = new Utils();

            return client.PingServer();
		}
	}
}

