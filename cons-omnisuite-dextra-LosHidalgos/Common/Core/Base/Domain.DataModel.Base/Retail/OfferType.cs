﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using LSRetail.Omni.Domain.DataModel.Base.Base;

namespace LSRetail.Omni.Domain.DataModel.Base.Retail
{
    [DataContract(Namespace = "http://lsretail.com/LSOmniService/Base/2017")]
    public enum OfferType
    {
        [EnumMember]
        General = 0,  // not a part of member mgt system
        [EnumMember]
        SpecialMember = 1, //member attribute based, specifically the member contact signed up for this (golf etc)
        [EnumMember]
        PointOffer = 2, //Points and Coupons, item Points and Coupons,  something you can pay with
        [EnumMember]
        Club = 3,      //Club and scheme, offer is part of member mgt, got offer since you were in club,scheme,account or contact
        [EnumMember]
        Unknown = 100,
    }

    //New Offer data structure, published offers
    [DataContract(Namespace = "http://lsretail.com/LSOmniService/Base/2017")]
    public enum OfferCode
    {
        [EnumMember]
        Promotion = 0,  //Promotion
        [EnumMember]
        Deal = 1,       //Deal
        [EnumMember]
        Multibuy = 2,     
        [EnumMember]
        MixAndMatch = 3,  
        [EnumMember]
        DiscountOffer = 4,
        [EnumMember]
        TotalDiscount = 5,
        [EnumMember]
        TenderType = 6,  
        [EnumMember]
        ItemPoint = 7,   
        [EnumMember]
        LineDiscount = 8,  
        [EnumMember]
        Coupon = 9,
        [EnumMember]
        Unknown = 100,
    }

    [DataContract(Namespace = "http://lsretail.com/LSOmniService/Base/2017")]
    public enum OfferDiscountType
    {
        [EnumMember]
        Promotion = 0,
        [EnumMember]
        Deal = 1,
        [EnumMember]
        Multibuy = 2,
        [EnumMember]
        MixMatch = 3,
        [EnumMember]
        DiscOffer = 4,
        [EnumMember]
        TotalDiscount = 5,
        [EnumMember]
        TenderType = 6,
        [EnumMember]
        ItemPoint = 7,
        [EnumMember]
        LineDiscount = 8,
        [EnumMember]
        Coupon = 9,
    }

    [DataContract(Namespace = "http://lsretail.com/LSOmniService/Base/2017")]
    public enum OfferDiscountLineType
    {
        [EnumMember]
        Item = 0,
        [EnumMember]
        ProductGroup = 1,
        [EnumMember]
        ItemCategory = 2,
        [EnumMember]
        All = 3,
        [EnumMember]
        SpecialGroup = 4,
    }
}

