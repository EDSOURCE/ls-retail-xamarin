namespace LSRetail.Omni.Domain.DataModel.Base.Menu
{
    public static class KitchenStatus
    {
        // (Not routed, Not Sent, Sent, PartiallySent, Voided (0,1,2,3,4))
        public const string NOT_ROUTED = "0";
        public const string NOT_SENT = "1";
        public const string SENT = "2";
        public const string PARTIALLY_SENT = "3";
        public const string VOIDED = "4";

        public static bool ShouldPromptSendToKitchen(string kitchenStatus)
        {
            switch (kitchenStatus)
            {
                case NOT_SENT:
                    return true;
                case PARTIALLY_SENT:
                    return true;
                default:
                    return false;
            }
        }
    }
}