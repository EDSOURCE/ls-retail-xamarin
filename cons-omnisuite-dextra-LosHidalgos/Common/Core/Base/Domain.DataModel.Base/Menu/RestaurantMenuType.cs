using LSRetail.Omni.Domain.DataModel.Base.Base;

namespace LSRetail.Omni.Domain.DataModel.Base.Menu
{
    public class RestaurantMenuType : Entity, IAggregateRoot
    {
        private string description;
        private string codeOnPos;
        private bool clearMenuType;
        private bool selected;
        private bool isMenuTypeNone;	// Is this the "none" menutype, i.e. a menutype that signifies no particular menutype

        #region Constructor

        public RestaurantMenuType() : this(null)
        {
        }

        public RestaurantMenuType(string id) : base(id)
        {
            description = string.Empty;
            codeOnPos = string.Empty;
            clearMenuType = false;
            selected = false;
            isMenuTypeNone = false;
        }

        #endregion 

        #region Properties

        public string Description
        {
            get { return this.description; }
            set { this.description = value; }
        }

        public string CodeOnPos
        {
            get { return this.codeOnPos; }
            set { this.codeOnPos = value; }
        }

        public bool ClearMenuType
        {
            get { return this.clearMenuType; }
            set { this.clearMenuType = value; }
        }

        public bool Selected
        {
            get { return this.selected; }
            set { this.selected = value; }
        }

        public bool IsMenuTypeNone
        {
            get { return this.isMenuTypeNone; }
            set { this.isMenuTypeNone = value; }
        }

        #endregion

        public RestaurantMenuType Clone()
        {
            return this.MemberwiseClone() as RestaurantMenuType;
        }

    }
}
