﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LSRetail.Omni.Domain.DataModel.Base.Retail;

namespace LSRetail.Omni.Domain.DataModel.Base.Utils
{
    public class ImageCache
    {
        public class ImageCacheItem
        {
            public string ItemId { get; set; }
            public string VariantId { get; set; }
            public ImageSize ImageSize { get; set; }
            public List<ImageView> Images { get; set; }
        }

        private List<ImageCacheItem> cachedImages = new List<ImageCacheItem>();
        private const int maxSize = 10000000;  //10MB
        private int totalSize;

        public void AddImageToCache(ImageCacheItem cacheItem)
        {
            lock (cachedImages)
            {
                var existingItem = cachedImages.FirstOrDefault(x => x.ItemId == cacheItem.ItemId && x.VariantId == cacheItem.VariantId);
                if (existingItem != null)
                {
                    if (existingItem.ImageSize.Width == cacheItem.ImageSize.Width && existingItem.ImageSize.Height == cacheItem.ImageSize.Height)
                    {
                        return;
                    }
                    else
                    {
                        RemoveItemFromCache(existingItem);
                    }
                }

                cachedImages.Add(cacheItem);

                foreach (var image in cacheItem.Images)
                {
                    totalSize += image.Image.Length * 3;
                }

                if (totalSize >= maxSize)
                {
                    while (totalSize >= maxSize && cachedImages.Count > 1)
                    {
                        var item = cachedImages[0];

                        RemoveItemFromCache(item);
                    }
                }
            }
        }

        public List<ImageView> GetItemFromCache(string itemId, string variantId, ImageSize imageSize)
        {
            return cachedImages?.FirstOrDefault(cacheItem => cacheItem.ItemId == itemId && cacheItem.VariantId == variantId && cacheItem.ImageSize?.Width == imageSize.Width && cacheItem.ImageSize?.Height == imageSize.Height)?.Images;
        }

        private void RemoveItemFromCache(ImageCacheItem cacheItem)
        {
            foreach (var itemImage in cacheItem.Images)
            {
                totalSize -= itemImage.Image.Length * 3;
            }

            cachedImages.Remove(cacheItem);
        }

        public void Clear()
        {
            cachedImages.Clear();
            totalSize = 0;
        }
    }
}
