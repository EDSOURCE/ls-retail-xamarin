﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using LSRetail.Omni.Domain.DataModel.Base.Retail;

namespace LSRetail.Omni.Domain.DataModel.Base.Replication
{
    [DataContract(Namespace = "http://lsretail.com/LSOmniService/Base/2017")]
    public class ReplDiscountResponse : IDisposable
    {
        public ReplDiscountResponse()
        {
            LastKey = string.Empty;
            MaxKey = string.Empty;
            RecordsRemaining = 0;
            Discounts = new List<ReplDiscount>();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (Discounts != null)
                    Discounts.Clear();
            }
        }

        [DataMember]
        public string LastKey { get; set; }
        [DataMember]
        public string MaxKey { get; set; }
        [DataMember]
        public int RecordsRemaining { get; set; }
        [DataMember]
        public List<ReplDiscount> Discounts { get; set; }
    }

    [DataContract(Namespace = "http://lsretail.com/LSOmniService/Pos/2017")]
    public class ReplDiscount : IDisposable
    {
        public ReplDiscount()
        {
            IsDeleted = false;
            PriorityNo = 0;
            ItemId = string.Empty;
            VariantId = string.Empty;
            CustomerDiscountGroup = string.Empty;
            LoyaltySchemeCode = string.Empty;
            FromDate = new DateTime(1900, 1, 1);
            ToDate = new DateTime(1900, 1, 1);
            ModifyDate = DateTime.Now;
            UnitOfMeasureId = string.Empty;
            MinimumQuantity = 0M;
            CurrencyCode = string.Empty;
            DiscountValue = 0M;
            OfferNo = string.Empty;
            StoreId = string.Empty;
            Type = 0; //Disc. Offer, Multibuy
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }

        [DataMember]
        public bool IsDeleted { get; set; }
        [DataMember]
        public string StoreId { get; set; }
        [DataMember]
        public int PriorityNo { get; set; }
        [DataMember]
        public string ItemId { get; set; }
        [DataMember]
        public string VariantId { get; set; }
        [DataMember]
        public string CustomerDiscountGroup { get; set; }
        [DataMember]
        public string LoyaltySchemeCode { get; set; }
        [DataMember]
        public DateTime FromDate { get; set; }
        [DataMember]
        public DateTime ToDate { get; set; }
        [DataMember]
        public string UnitOfMeasureId { get; set; }
        [DataMember]
        public decimal MinimumQuantity { get; set; }
        [DataMember]
        public string CurrencyCode { get; set; }
        [DataMember]
        public decimal DiscountValue { get; set; }
        [DataMember]
        public string OfferNo { get; set; }
        [DataMember]
        public DateTime ModifyDate { get; set; }
        [DataMember]
        public int Type { get; set; }
    }
}
