using System.Collections.Generic;
using LSRetail.Omni.Domain.DataModel.Loyalty.Transactions;

namespace LSRetail.Omni.Domain.Services.Loyalty.Transactions
{
    public interface ITransactionRepository
    {
        /// <summary>
        /// Gets a list of transaction headers
        /// </summary>
        /// <param name="contactId">The user the transactions belong to</param>
        /// <param name="numerOfTransactionsToReturn">The number of most recent transactions to return</param>
        /// <returns>
        /// A list of transaction headers.  To get the full details for a transaction a separate function must be called
        /// for that specific transaction.
        /// </returns>
        List<LoyTransaction> GetTransactionHeaders(string contactId, int numerOfTransactionsToReturn);

        /// <summary>
        /// Gets a transaction header
        /// </summary>
        /// <param name="storeId">Store Id</param>
        /// <param name="terminalId">Terminal Id</param>
        /// <param name="transactionId">Transaction Id</param>
        /// <param name="includeLines">Include transaction detail lines</param>
        /// <returns>
        /// A transaction header with the sale and tender lines. 
        /// </returns>
        LoyTransaction GetTransactionById(string storeId, string terminalId, string transactionId, bool includeLines);

        /// <summary>
        /// Gets a list of transaction headers
        /// </summary>
        /// <param name="contactId">The user the transactions belong to</param>
        /// <param name="itemSerach">The search string used as criteria for item description</param>
        /// <param name="numerOfTransactionsToReturn">The number of most recent transactions to return</param>
        /// <param name="includeLines">Include transaction detail lines</param>
        /// <returns>
        /// A list of transaction headers.  To get the full details for a transaction a separate function must be called
        /// for that specific transaction.
        /// </returns>
        List<LoyTransaction> GetTransactionSearch(string contactId, string itemSerach, int numerOfTransactionsToReturn, bool includeLines);
    }
}
