using System.Collections.Generic;
using System.Threading.Tasks;
using LSRetail.Omni.Domain.DataModel.Loyalty.Transactions;

namespace LSRetail.Omni.Domain.Services.Loyalty.Transactions
{
    public class TransactionService
    {
        private ITransactionRepository iTransactionRepository;

        public TransactionService(ITransactionRepository iRepo)
        {
            iTransactionRepository = iRepo;
        }

        public List<LoyTransaction> GetTransactionHeaders(string contactId, int numerOfTransactionsToReturn)
        {
            return iTransactionRepository.GetTransactionHeaders(contactId, numerOfTransactionsToReturn);
        }

        public LoyTransaction GetTransactionById(string storeId, string terminalId, string transactionId, bool includeLines)
        {
            return iTransactionRepository.GetTransactionById(storeId, terminalId, transactionId, includeLines);
        }

        public List<LoyTransaction> GetTransactionSearch(string contactId, string itemSerach, int numerOfTransactionsToReturn, bool includeLines)
        {
            return iTransactionRepository.GetTransactionSearch(contactId, itemSerach, numerOfTransactionsToReturn, includeLines);
        }

        public async Task<List<LoyTransaction>> GetTransactionHeadersAsync(string contactId, int numerOfTransactionsToReturn)
        {
            return await Task.Run(() => GetTransactionHeaders(contactId, numerOfTransactionsToReturn));
        }

        public async Task<LoyTransaction> GetTransactionByIdAsync(string storeId, string terminalId, string transactionId, bool includeLines)
        {
            return await Task.Run(() => GetTransactionById(storeId, terminalId, transactionId, includeLines));
        }

        public async Task<List<LoyTransaction>> GetTransactionSearchAsync(string contactId, string itemSerach, int numerOfTransactionsToReturn, bool includeLines)
        {
            return await Task.Run(() => GetTransactionSearch(contactId, itemSerach, numerOfTransactionsToReturn, includeLines));
        }
    }
}
