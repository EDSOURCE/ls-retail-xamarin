﻿using System.Collections.Generic;

using LSRetail.Omni.Infrastructure.Data.Omniservice.Base;
using LSRetail.Omni.Domain.DataModel.Loyalty.Transactions;
using LSRetail.Omni.Domain.Services.Loyalty.Transactions;

namespace LSRetail.Omni.Infrastructure.Data.Omniservice.Loyalty.Orders
{
    public class TransactionRepository : BaseRepository, ITransactionRepository
    {
        public List<LoyTransaction> GetTransactionHeaders(string contactId, int maxNumberOfTransactions)
        {
            string methodName = "TransactionHeadersGetByContactId";
            var jObject = new { contactId = contactId, maxNumberOfTransactions = maxNumberOfTransactions };
            return base.PostData<List<LoyTransaction>>(jObject, methodName);
        }

        public LoyTransaction GetTransactionById(string storeId, string terminalId, string transactionId, bool includeLines)
        {
            string methodName = "TransactionGetById";
            var jObject = new { storeId = storeId, terminalId = terminalId, transactionId = transactionId, includeLines = includeLines.ToString().ToLower() };
            return base.PostData<LoyTransaction>(jObject, methodName);
        }

        public List<LoyTransaction> GetTransactionSearch(string contactId, string itemSearch, int maxNumberOfTransactions, bool includeLines)
        {
            string methodName = "TransactionsSearch";
            var jObject = new { contactId = contactId, itemSearch = itemSearch, maxNumberOfTransactions = maxNumberOfTransactions, includeLines = includeLines.ToString().ToLower() };
            return base.PostData<List<LoyTransaction>>(jObject, methodName);
        }
    }
}
