﻿using System.Collections.Generic;
using LSRetail.Omni.Domain.DataModel.Base.Retail;
using LSRetail.Omni.Domain.Services.Base.PublishedOffers;
using LSRetail.Omni.Infrastructure.Data.Omniservice.Base;

namespace LSRetail.Omni.Infrastructure.Data.Omniservice.Loyalty.PublishedOffers
{
    public class OfferRepository : BaseRepository, IPublishedOfferRepository
    {
        public List<PublishedOffer> GetPublishedOffers(string cardId, string itemId, string storeId)
        {
            //if itemId is empty then no filtering is done.
            string methodName = "PublishedOffersGetByCardId";
            var jObject = new { cardId = cardId, itemId = itemId, storeId = storeId };
            return base.PostData<List<PublishedOffer>>(jObject, methodName);
        }
    }
}
