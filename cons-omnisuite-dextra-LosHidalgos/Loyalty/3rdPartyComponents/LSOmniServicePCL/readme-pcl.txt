LSOmniServicePCL usage.  
See working test projects under \LSOmniService\trunk\PclTest\Android, iOS, Windows
---

Add Reference to these DLLs
\LSOmniService\trunk\3rdPartyComponents\LSOmniServicePCL\LSOmniServicePCL.dll
\LSOmniService\trunk\3rdPartyComponents\LSOmniServicePCL\Newtonsoft.Json.dll

Android
\LSOmniService\trunk\3rdPartyComponents\LSOmniServicePCL\Xam.Plugins.Settings\lib\MonoAndroid10
Refractored.Xam.Settings.Abstractions.dll
Refractored.Xam.Settings.dll
\LSOmniService\trunk\3rdPartyComponents\LSOmniServicePCL\PCLCrypto\PCLCrypto.0.5.2.14286\lib\monoandroid\PCLCrypto.dll
\LSOmniService\trunk\3rdPartyComponents\LSOmniServicePCL\PCLCrypto\PCLCrypto.2.0.5.14286\lib\portable-windows8+net40+sl5+wp8+wpa81+wp81+MonoAndroid+MonoTouch\validation.dll

iOS
\LSOmniService\trunk\3rdPartyComponents\LSOmniServicePCL\Xam.Plugins.Settings\lib\MonoTouch10
Refractored.Xam.Settings.Abstractions.dll
Refractored.Xam.Settings.dll
\LSOmniService\trunk\3rdPartyComponents\LSOmniServicePCL\PCLCrypto\PCLCrypto.0.5.2.14286\lib\monotouch\PCLCrypto.dll
\LSOmniService\trunk\3rdPartyComponents\LSOmniServicePCL\PCLCrypto\PCLCrypto.2.0.5.14286\lib\portable-windows8+net40+sl5+wp8+wpa81+wp81+MonoAndroid+MonoTouch\validation.dll
