using System;
using UIKit;

namespace Presentation.Utils
{
	public class AppColors
	{
		// LS Retail blue: (40, 64, 114)
		// Old Loyalty Green: (64, 130, 109)
		public static UIColor PrimaryColor = UIColor.FromRGB(29, 169, 157); // Buttercup: (232,28,127); //(171,91,132); //(248, 202, 224) // KFC: (163, 8, 12);
		public static UIColor TextColor  = UIColor.FromRGB(40, 64, 114);
		public static UIColor MediumGray = UIColor.FromRGB (101, 109, 124);
		public static UIColor LightGrey = UIColor.FromRGB (224, 224, 224);  //Light Green
		public static UIColor AttentionRed = UIColor.FromRGB(218, 44, 67);

		public static UIColor SoftWhite = UIColor.FromRGB(245, 245, 245);
		public static UIColor BackgroundGray = UIColor.FromRGB(237, 237, 237);
		public static UIColor TransparentBlack = UIColor.FromRGBA (0, 0, 0, 80);
		public static UIColor TransparentBlack2 = UIColor.FromRGBA(0, 0, 0, 140);
		public static UIColor TransparentBlack3 = UIColor.FromRGBA(0, 0, 0, 200);
		public static UIColor TransparentWhite = UIColor.FromRGBA(255,255,255,200);
		public static UIColor TransparentWhite2 = UIColor.FromRGBA(255,255,255,100);
		public static UIColor TransparentWhite3 = UIColor.FromRGBA(255,255,255,235); //225 //245 
	}
}
