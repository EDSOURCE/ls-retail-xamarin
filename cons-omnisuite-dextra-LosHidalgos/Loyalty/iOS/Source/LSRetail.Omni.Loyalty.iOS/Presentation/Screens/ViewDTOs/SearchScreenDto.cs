﻿using System;
using System.Collections.Generic;

namespace Presentation
{
	public class SearchScreenDto
	{
		public string Description { get; set; }
		public List<Object> Data { get; set; }
	}
}

