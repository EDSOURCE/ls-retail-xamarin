//GENERAL
"General_OK" = "OK"; //[Term:456]
"General_Cancel" = "Avbryt"; //[Term:367]
"General_Back" = "Tilbake"; //[Term:363]
"General_Error" = "Feil";
"General_ErrorTryAgain" = "Feil, vennligst prøv igjen";
"General_Yes" = "Ja"; //[Term:514]
"General_No" = "Nei"; //[Term:454]
"General_Info" = "Info";
"General_Done" = "Ferdig";
"General_Apply" = "Bruk";
"General_Retry" = "Prøv på nytt";
"General_Refresh" = "Forny";
"General_Attention" = "Advarsel";
"General_Proceed" = "Fortsett"; //[Term:24117]
"General_GetDataErrorMessage" = "Noe gikk galt...\r\nTrykk forny for å prøve igjen"; //[Term:24121]

//Base Model UI Exceptions
"Model_AuthenticationFailedException" = "Feil brukernavn eller passord. Vennligst prøv igjen.";
"Model_GenericException" = "Feil, vennligst prøv igjen";
"Model_NetworkException" = "Nettverksfeil";
"Model_DeviceBlocked" = "Denne enheten har blitt sperret";
"Model_AccessNotAllowed" = "Ingen tilgang"; //[Term:24103]
"Model_EmailExists" = "E-post finnes ikke";
"Model_SecurityToken" = "Ingen tilgang"; //[Term:24103]
"Model_InvalidPassword" = "Feil passord";
"Model_InvalidOldPassword" = "Ugyldig gammelt passord";
"Model_InvalidUsername" = "Ugyldig brukernavn";
"Model_ItemNotFound" = "Vare ikke funnet";
"Model_EmailInvalidException" = "E-post ugyldig";

//Welcome Screen
"Welcome_ApplicationTitle" = "Kvalitetsbutikk";
"Welcome_Title" = "Velkommen";
"Welcome_WelcomeText" = "To get started, tap the button below to log in or to create a new account and configure your personal profile.

Being a member allows you to benefit from personal offers and coupons as well as collecting points, which can be used for tendering during your shopping experience.";

// Home
"Home_Home" = "Hjem";
"Home_HomeScreen" = "Hjemskjerm";
"Home_Points_Lowercase" = "poeng";

// History
"History_History" = "Historikk";
"History_Item_Lowercase" = "vare";
"History_Items_Lowercase" = "varer";
"History_NoData" = "Ingen tidligere transaksjoner tilgjengelige"; //[Term:24137]
"History_SearchScreen" = "Handlehistorikk fra ";


"TransactionView_Expire" = "Utløper: ";
"TransactionView_Total" = "I alt:";
"TransactionView_Discount" = "Rabatt:";
"TransactionView_Vat" = "MVA:";
"TransactionView_Net" = "Netto totalt:"; //[Term:24131]
"TransactionView_ItemName" = "Varenavn";
"TransactionView_Qty" = "Ant.";
"TransactionView_Amount" = "Beløp";
"TransactionView_TransactionUnavailable" = "Transaksjon ikke tilgjengelig"; //[Term:24127]
"TransactionView_TransactionUnavailableMsg" = "Denne transaksjonen er ikke tilgjengelig for øyeblikket"; //[Term:24122]

// Location/Store/Restaurant details
"Location_Locations" = "Lokasjoner";
"Locations_MapOfAllLocations" = "Kart over alle lokasjoner";
"Location_Details_Phone" = "Telefon";
"Location_Details_OpeningHours" = "Åpningstider";
"Location_CouldNotGetLocation" = "Kunne ikke finne nåværende posisjon"; //[Term:24130]
"Location_CouldNotGetLocationInstructions" = "Forsikre deg om at stedstjenester er aktivert. For å aktivere, går du til innstillinger og skrur på stedstjenester for denne appen.";

// Offers & coupons
"OffersAndCoupons_OffersAndCoupons" = "Tilbud & kuponger";
"OffersAndCoupons_Offers" = "Tilbud"; //[Term:8320]
"OffersAndCoupons_Coupons" = "Kuponger"; //[Term:22509]
"OffersAndCoupons_NoOffersAndCoupons" = "Det er ingen tilbud eller kuponger tilgjengelige";
"OffersAndCoupons_NoOffers" = "Det er ingen tilbud tilgjengelige";
"OffersAndCoupons_NoCoupons" = "Det er ingen kuponger tilgjengelige";
"OffersAndCoupons_NoOffersPleaseLogIn" = "Ingen tilbud tilgjengelige, prøv å logge inn"; //[Term:24136]
"OffersAndCoupons_NoCouponsPleaseLogIn" = "Ingen kuponger tilgjengelige, prøv å logge inn"; //[Term:24132]
"OffersAndCoupons_PointOffers" = "Poengtilbud";
"OffersAndCoupons_MemberOffers" = "Medlemstilbud";
"OffersAndCoupons_GeneralOffers" = "Generelle tilbud"; //[Term:24116]

// Offer details

// Coupon details
"Coupon_Details_Close" = "Lukk";
"Coupon_Details_QRCode" = "QR-kode"; //[Term:24114]
"Coupon_Details_PleaseScan" = "Vennligst skann QR-kode";
"Coupon_Details_AddToBasket" = "Legg til kurv";
"Coupon_Details_RemoveFromBasket" = "Fjern fra kurv";
"Coupon_Details_ValidUntil" = "Gyldig til";

// Checkout
"Checkout_Checkout" = "Kasse"; //[Term:24111]
"Checkout_PlaceOrder" = "Plasser bestilling"; //[Term:24113]
"Checkout_ClearBasket" = "Tøm kurv"; //[Term:24139]
"Checkout_RemoveItem" = "Fjern vare";
"Checkout_Total" = "I alt";
"Checkout_AreYouSureClearBasket" = "Er du sikker på at du vil tømme kurven?"; //[Term:24106]
"Checkout_AreYouSurePlaceOrder" = "Er du sikker på at du vil gjennomføre bestillingen?"; //[Term:24108]
"Checkout_AreYouSureRemoveItem" = "Er du sikker på at du vil slette denne varen?"; //[Term:15254]
"Checkout_ErrorPlacingOrder" = "Kunne ikke gjennomføre bestillingen.\r\nVenligst prøv igjen.";
"Checkout_ThankYou" = "Takk!";
"Checkout_OrderProcessed" = "Ordren din har blitt behandlet uten problemer."; //[Term:24126]
"Checkout_ShippingMethod" = "Velg leveringsmetode"; //[Term:24120]
"Checkout_HomeDelivery" = "Hjem Levering";
"Checkout_HomeDeliveryTitle" = "Hjem Levering - Demoside";
"Checkout_HomeDeliveryMsg" = "Denne prosessen må tilpasses spesielt for hver region";
"Checkout_ConfirmOrder" = "Bekreft bestilling"; //[Term:24141]
"Checkout_Verify" = "Vennligst bekreft ordren din før du fortsetter"; //[Term:24129]
"Checkout_SubTotal" = "Totalsum:";
"Checkout_Discount" = "Rabatt:";
"Checkout_VAT" = "MVA:";
"Checkout_HomeDeliveryDescription" = "La oss  sende bestillingen";
"Checkout_ClickCollectDescription" = "Hent ordren i en valgfri butikk på en tid du selv ønsker";

//Click & Collect
"ClickCollect_NotAvailableTitle" = "Ikke tilgjengelig i denne butikken"; //[Term:24138]
"ClickCollect_NotAvailableMsg" = "Vare(r) ikke tilgjengelig, vennligst prøv en annen butikk";
"ClickCollect_Stores" = "Butikker"; //[Term:14629]
"ClickCollect_Shipping" = "Levering";
"ClickCollect_ClickCollect" = "Trykk & Innkrev";
"ClickCollect_SendOrderDetails" = "Ordredetaljer vil sendes til:";
"ClickCollect_Attention" = "Advarsel!";
"ClickCollect_Proceed" = "Trykk \"Fortsett\" dersom du ønsker å fortsette på ordren under."; //[Term:24140]
"ClickCollect_ItemNotAvailable" = "Denne varen er ikke tilgjengelig på ";
"ClickCollect_ItemsNotAvailable" = "Disse varene er ikke tilgjengelige på ";
"ClickCollect_Qty" = "Ant:";
"ClickCollect_StoreDistance" = "km vekk herfra";

// Account
"Account_Account" = "Konto";
"Account_CreateAccount" = "Opprett konto";
"Account_ManageAccount" = "Administrer konto";
"Account_UpdateAccount" = "Oppdater konto"; //[Term:24123]
"Account_Login" = "Logg inn";
"Account_Logout" = "Logg ut";
"Account_AreYouSureLogout" = "Er du sikker på at du vil logge deg ut?"; //[Term:24107]
"Account_Points_Lowercase" = "poeng";
"Account_Email" = "E-post";
"Account_Password" = "Passord";
"Account_ConfirmPassword" = "Bekreft passord";
"Account_PleaseFillAllFields" = "Vennligst fyll ut alle feltene"; //[Term:24142]
"Account_InvalidEmail" = "Ugyldig e-post";
"Account_EmptyUsernameError" = "Brukernavn kan ikke stå tomt";
"Account_ForgotPassword" = "Glemt passordet?";
"Account_PasswordMismatch" = "Passordet samsvarer ikke";
"Account_ResetPasswordTitle" = "Tilbakestill passord"; //[Term:24128]
"Account_NewPassword" = "Nytt passord"; //[Term:447]
"Account_ConfirmNewPassword" = "Bekreft nytt passord";
"Account_OldPassword" = "Gammelt passord";
"Account_PasswordSuccess" = "Passordet ble endret vellykket";
"Account_AccountUpdated" = "Kontoen ble oppdatert uten problemer";
"Account_ResetPasswordMsg" = "Oppgi brukernavnet ditt og og få tilsendt et nytt passord på E-post";
"Account_ProvidedPasswordsDontMatch" = "Det passordet som ble oppgitt samsvarer ikke";
"Account_AlreadyHaveAnAccountLogIn" = "Har du allerede en konto? Logg inn."; //[Term:24115]
"Account_NewUserCreateAnAccount" = "Ny bruker? Opprett konto.";
"Account_Username" = "Brukernavn";
"Account_Name" = "Navn";
"Account_AddressOne" = "Adresse 1"; //[Term:24104]
"Account_AddressTwo" = "Adresse 2"; //[Term:24105]
"Account_City" = "By";
"Account_State" = "Stat";
"Account_Country" = "Land";
"Account_PostCode" = "Postnummer";
"Account_Change" = "Endre";
"Account_ChangePassword" = "Endre passord";
"Account_PhoneNumber" = "Telefon";
"Account_Optional" = "Valgfritt";
"Account_ChooseProfiles" = "Velg profiler";
"Account_EmptyError" = "kan ikke stå tomt";
"Account_Continue" = "Fortsett"; //[Term:3618]
"Account_ErrorChangePassword" = "Feil ved bytting av passord";
"Account_Member" = "medlem";
"Account_Profiles" = "Velg profiler";
"Account_SignUp" = "Meld deg på";

//Notifications
"Notification_RemoveNotifications" = "Fjern alle varsler";
"Notification_AreYouSureRemoveNotifications" = "Er du sikker på at du vil slette alle varsler?"; //[Term:24109]
"Notification_NoNotifications" = "Ingen varsler"; //[Term:24135]

// QR Code
"QRCode_QRCode" = "QR-kode"; //[Term:24114]
"QRCode_Identify" = "Vis denne QR-koden for å identifisere deg";

//Barcode Scanner
"ScanBarcode_btnLight" = "Lys";
"ScanBarcode_Error" = "Feilet med å starte scanner"; //[Term:24118]

// Slideout basket
"SlideoutBasket_Checkout" = "Kasse"; //[Term:24111]
"SlideoutBasket_YourOrder" = "Din ordre";
"SlideoutBasket_BasketEmpty" = "Kurven din er tom! Legg til varer, da vil de vises her.";
"SlideoutBasket_BasketEmptyAndNotLoggedIn" = "Kurven din er tom! Logg inn og begynn med å legge til varer.";
"SlideoutBasket_Total" = "I alt";
"SlideoutBasket_Coupons" = "Kuponger"; //[Term:22509]
"SlideoutBasket_Offers" = "Tilbud"; //[Term:8320]
"SlideoutBasket_QuantityAbbreviation" = "Ant.";
"SlideoutBasket_Multiplier" = "x";
"SlideoutBasket_ExtraLineFormatStringModifier" = "{0}";
"SlideoutBasket_ExtraLineFormatStringPlus" = "+ {0} {1}";
"SlideoutBasket_ExtraLineFormatStringMinus" = "- {0} {1}";
"SlideoutBasket_ExtraLineFormatStringPlusUom" = "+ {0} {1} {2}";
"SlideoutBasket_ExtraLineFormatStringMinusUom" = "- {0} {1} {2}";

// Basket
"Basket_Checkout" = "Kasse"; //[Term:24111]

// Edit basket item
"EditBasketItem_EditItem" = "Rediger vare";

// Add to basket
"AddToBasket_AddToBasket" = "Legg til kurv";

//Slideout Menu
"SlideoutMenu_Items" = "Varer";
"SlideoutMenu_ScanBarcode" = "Skann strekkode";
"SlideoutMenu_Notifications" = "Varsler"; //[Term:24112]
"SlideoutMenu_WishList" = "Ønskeliste";
"SlideoutMenu_Search" = "Søk";
"SlideoutMenu_ContactUs" = "Kontakt oss";

//Item
"Item_NotFoundTitle" = "Vare ikke funnet";
"Item_NotFoundMsg" = "Denne varen er ikke i databasen vår";
"Item_Variant" = "Variant";
"Item_UOM" = "Enh.";

// Item details
"ItemDetails_AddToBasket" = "Legg til kurv";
"ItemDetails_Modify" = "Endre";
"ItemDetails_Quantity" = "Antall";
"ItemDetails_Per" = "pr.";
"ItemDetails_ItemAddedToWishList" = "ble lagt til i Ønskelisten!";
"ItemDetails_ItemNotInStock" = "Vare ikke på lager";
"ItemDetails_NoItems" = "Ingen varer å vise."; //[Term:24134]
"ItemDetails_Availability" = "Se tilgjengelighet"; //[Term:24119]
"ItemDetails_SelectVariant" = "Velg variant"; //[Term:23628]

// Image zoom screen

// Transaction details
"TransactionDetails_Multiplier" = "x";
"TransactionDetails_Transaction" = "Transaksjon"; //[Term:8012]
"TransactionDetails_ErrorAddingItemToBasket" = "Kunne ikke legge til vare i kurven";
"TransactionDetails_ErrorAddingSomeItemsToBasket" = "Kunne ikke legge til alle varene i kurven";

//Wish List
"WishList_WishList" = "Ønskeliste";
"WishList_RemoveFromWishList" = "Fjern fra Ønskeliste";
"WishList_AreYouSureRemoveItem" = "Er du sikker på at du vil fjerne denne varen fra Ønskelisten din?"; //[Term:24110]
"WishList_AddToBasket" = "Legg Ønskeliste til i Kurv";
"WishList_AddToWishList" = "Legg til i Ønskeliste";
"WishList_NoItems" = "Ingen varer i Ønskelisten."; //[Term:24133]

//Notifications
"Notifications_Notifications" = "Varsler"; //[Term:24112]
"Notifications_DeleteAll" = "Slett alle varsler";

//Search
"Search_NoResults" = "Søket ga ingen resultater";
"Search_Instruction" = "Bruk søkebaren for generelt søk."; //[Term:24124]
"SearchScreen_Coupons" = "Kuponger"; //[Term:22509]
"SearchScreen_Notifications" = "Varsler"; //[Term:24112]
"SearchScreen_Offers" = "Tilbud"; //[Term:8320]
"SearchScreen_Stores" = "Butikker"; //[Term:14629]

// Map view
"MapView_AllLocations" = "Alle lokasjoner";

// Hidden settings
"Hidden_Settings_HiddenSettings" = "Skjulte Innstillinger";
"Hidden_Settings_URL" = "URL"; //[Term:2282]
"Hidden_Settings_Resource" = "Ressurs";
"Hidden_Settings_WebserviceChanged" = "Nettjener endret"; //[Term:24125]
"Hidden_Settings_WebserviceChangeSuccessMessage" = "ATTENTION\r\n Restart appen HELT ved å dobbelttrykke på hjem knappen og sveipe appen til siden. Deretter åpne appen på nytt.";

// Custom more controller

// Weekdays
"Weekday_Monday" = "Mandag";
"Weekday_Tuesday" = "Tirsdag"; //[Term:3815]
"Weekday_Wednesday" = "Onsdag"; //[Term:3810]
"Weekday_Thursday" = "Torsdag"; //[Term:3819]
"Weekday_Friday" = "Fredag";
"Weekday_Saturday" = "Lørdag"; //[Term:3826]
"Weekday_Sunday" = "Søndag"; //[Term:3821]

// SearchPopUp

//ChangeVariantQtyPopUpView

//Related
