using System;
using Presentation.Utils;
using System.Collections.Generic;
using System.Threading.Tasks;
using LSRetail.Omni.Domain.DataModel.Loyalty.Orders;
using LSRetail.Omni.Domain.DataModel.Loyalty.Baskets;
using LSRetail.Omni.Domain.Services.Loyalty.Baskets;
using LSRetail.Omni.Infrastructure.Data.Omniservice.Loyalty.Baskets;

namespace Presentation.Models
{
    public class ClickCollectModel : BaseModel
    {
        private BasketService clickCollectService;
        private IBasketRepository clickCollectRepository;

        public ClickCollectModel()
        {
            this.clickCollectRepository = new BasketsRepository();
            this.clickCollectService = new BasketService(this.clickCollectRepository);
        }

        public async void CheckAvailability(string storeId, Action<OneList, List<OneListItem>> onSuccess, Action onFailure)
        {
            string guid = Guid.NewGuid().ToString();

            OrderAvailabilityRequest request = new OrderAvailabilityRequest()
            {
                Id = guid,
                StoreId = storeId,
                CardId = AppData.Device.UserLoggedOnToDevice.Card.Id,
                //ItemNumberType = 0,  // 0 refers to "ItemNo" - this should not change
                SourceType = SourceType.LSOmni // 1 refers to "LS Omni
            };

            int lineNo = 1;
            foreach (OneListItem basketItem in AppData.Device.UserLoggedOnToDevice.Basket.Items)
            {
                OrderLineAvailability lineRequest = new OrderLineAvailability()
                {
                    OrderId = guid,
                    LineNumber = lineNo,
                    ItemId = basketItem.Item.Id,
                    VariantId = (basketItem.VariantReg != null ? basketItem.VariantReg.Id : string.Empty),
                    UomId = (basketItem.UnitOfMeasure != null ? basketItem.UnitOfMeasureId : string.Empty),
                    Quantity = basketItem.Quantity,
                    LineType = LineType.Item
                };

                request.OrderLineAvailabilityRequests.Add(lineRequest);
                lineNo += 1;
            }
            try
            {
                List<OrderLineAvailability> orderAvailability = await this.clickCollectService.OrderAvailabilityCheckAsync(request);
                if (orderAvailability != null)
                {
                    bool allItemsAvailable = true;

                    //if not all items are available, we will return new basket - with updated quantity
                    OneList newBasket = CreateNewBasket();
                    List<OneListItem> unavailableItems = new List<OneListItem>();
                    foreach (var orderLine in orderAvailability)
                    {
                        OneListItem basketItem = AppData.Device.UserLoggedOnToDevice.Basket.Items[orderLine.LineNumber - 1];
                        if (orderLine.Quantity < basketItem.Quantity)
                        {
                            allItemsAvailable = false;

                            if (orderLine.Quantity < 0)
                                orderLine.Quantity = 0;

                            unavailableItems.Add(new OneListItem(basketItem.Id)
                            {
                                Item = basketItem.Item,
                                Quantity = basketItem.Quantity - orderLine.Quantity,
                                UnitOfMeasure = basketItem.UnitOfMeasure,
                                VariantReg = basketItem.VariantReg,

                                Amount = basketItem.Amount,
                                NetAmount = basketItem.NetAmount,
                                TaxAmount = basketItem.TaxAmount,
                                Price = basketItem.Price
                            });

                            newBasket.Items[orderLine.LineNumber - 1].Quantity = orderLine.Quantity;
                        }
                    }

                    // if all items are available, we don't return any basket, the user just needs to confirm the original basket
                    if (allItemsAvailable)
                    {
                        onSuccess(null, null);
                    }
                    else
                    {
                        //remove all items that are not available at all
                        newBasket.Items.RemoveAll(x => x.Quantity <= 0);
                        onSuccess(newBasket, unavailableItems);
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex, "ClickCollectModel.CheckAvailability()", true);

            }
        }

        public async Task<Order> CreateOrder(string storeId, string email, OneList basket)
        {
            string guid = Guid.NewGuid().ToString();
            try
            {
                Order order = await this.clickCollectService.OrderCreateAsync(this.clickCollectService.CreateOrderForCAC(basket, AppData.Device.UserLoggedOnToDevice.Id, AppData.Device.UserLoggedOnToDevice.Card.Id, storeId, email));
                return order;
            }
            catch (Exception ex)
            {
                HandleException(ex, "ClickCollectModel.CreateOrder()", true);
                return null;
            }
        }

        private OneList CreateNewBasket()
        {
            List<OneListItem> basketItems = new List<OneListItem>();
            foreach (var item in AppData.Device.UserLoggedOnToDevice.Basket.Items)
            {
                OneListItem basketItem = new OneListItem(item.Id)
                {
                    Item = item.Item,
                    Quantity = item.Quantity,
                    UnitOfMeasure = item.UnitOfMeasure,
                    VariantReg = item.VariantReg,
                    Amount = item.Amount,
                    NetAmount = item.NetAmount,
                    TaxAmount = item.TaxAmount,
                    Price = item.Price
                };
                basketItems.Add(basketItem);
            }

            var newBasket = new OneList(AppData.Device.UserLoggedOnToDevice.Basket.Id, basketItems, false);
            newBasket.TotalNetAmount = AppData.Device.UserLoggedOnToDevice.Basket.TotalNetAmount;
            newBasket.TotalTaxAmount = AppData.Device.UserLoggedOnToDevice.Basket.TotalTaxAmount;
            newBasket.TotalAmount = AppData.Device.UserLoggedOnToDevice.Basket.TotalAmount;
            newBasket.State = BasketState.Dirty;
            return newBasket;
        }
    }
}
