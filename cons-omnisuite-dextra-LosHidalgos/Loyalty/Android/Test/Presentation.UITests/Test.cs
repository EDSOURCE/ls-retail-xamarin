﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using NUnit.Framework;
using Xamarin.UITest;

namespace Presentation.UITests
{
    /*[TestFixture]
    public class Test
    {
        private IApp app;

        [SetUp]
        public void Setup()
        {
            app = ConfigureApp.Android.ApkFile(@"C:\Users\FinnurEB\Desktop\Apps\20150107\LoyaltyNRF.apk").StartApp();
        }

        [Test]
        public async void Login_Error_DisplayErrorMessage()
        {
            await LoginWrongPassword();
        }

        [Test]
        public async void Login_Success()
        {
            await Login();
            await AddLindaSkirtToBasket();
            app.Repl();
        }

        private async Task LoginWrongPassword()
        {
            app.Tap(x => x.Marked("WelcomeViewLoginButton"));
            app.WaitForElement(x => x.Marked("LoginLogo"));
            app.EnterText(x => x.Marked("LoginViewUsernameField"), "tom");
            app.EnterText(x => x.Marked("LoginViewPasswordField"), "tom");
            app.Tap(x => x.Marked("LoginViewLoginButton"));

            //await Task.Delay(TimeSpan.FromSeconds(10));

            app.WaitForElement(x => x.Marked("LoginViewErrorText"));
            //app.WaitForElement(x => x.Marked("BaseActivityScreenDrawerContainer"));
        }

        private async Task Login()
        {
            app.Tap(x => x.Marked("WelcomeViewLoginButton"));
            app.WaitForElement(x => x.Marked("LoginLogo"));
            app.EnterText(x => x.Marked("LoginViewUsernameField"), "tom");
            app.EnterText(x => x.Marked("LoginViewPasswordField"), "tom.1");
            app.Tap(x => x.Marked("LoginViewLoginButton"));

            //await Task.Delay(TimeSpan.FromSeconds(10));

            app.WaitForElement(x => x.Marked("DrawerMenuListItemTitle"));
        }

        private async Task AddLindaSkirtToBasket()
        {
            app.Tap(x => x.Text("Items").Marked("DrawerMenuListItemTitle"));
            app.Tap(x => x.Marked("ItemCategoryListItemDescription").Index(3));
            app.Tap(x => x.Marked("ItemCategoryListItemDescription").Index(5));
            app.Tap(x => x.Marked("BaseCardLayoutPrimaryText").Index(2));
            app.ScrollDown();
            app.Tap(x => x.Marked("ItemViewAddToBasket"));
        }
    }*/
}
