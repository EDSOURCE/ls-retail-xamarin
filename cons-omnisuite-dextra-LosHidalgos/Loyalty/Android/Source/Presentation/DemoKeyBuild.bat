@REM File to be deleted
SET FolderToDelete="bin\Release"
SET FileToDelete="Loyalty.apk"
 
@Try to delete the file only if it exists
IF EXIST %FileToDelete% del /F %FileToDelete%
If Exist %FolderToDelete% rd /q/s %FolderToDelete%
 
@REM If the file wasn't deleted for some reason, stop and error
IF EXIST %FileToDelete% exit 1
IF EXIST %FolderToDelete% exit 1

msbuild /t:SignAndroidPackage Presentation.csproj /p:Configuration=Release
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ../../../../../Tools/Vault/"Android Keystore"/Demo/LSRAndroidKey.keystore -storepass .,lsR123 -keypass .,lsR123 bin/Release/lsretail.omni.loyalty.android.apk LSRAndroidReleaseKey
jarsigner -verify -verbose -certs bin/Release/lsretail.omni.loyalty.android.apk
zipalign -v 4 bin/Release/lsretail.omni.loyalty.android.apk Loyalty.apk