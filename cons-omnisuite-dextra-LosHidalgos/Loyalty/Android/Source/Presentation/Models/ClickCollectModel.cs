using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Android.Content;
using Android.Widget;
using LSRetail.Omni.Domain.DataModel.Loyalty.Baskets;
using LSRetail.Omni.Domain.DataModel.Loyalty.Orders;
using LSRetail.Omni.Domain.Services.Loyalty.Baskets;
using LSRetail.Omni.Infrastructure.Data.Omniservice.Loyalty.Baskets;
using Presentation.Util;

namespace Presentation.Models
{
    public class ClickCollectModel : BaseModel
    {
        private BasketService service;
        private BasketModel basketModel;

        public ClickCollectModel(Context context, IRefreshableActivity refreshableActivity) : base(context, refreshableActivity)
        {
            basketModel = new BasketModel(context, refreshableActivity);
        }

        public async Task<List<OrderLineAvailability>> OrderAvailabilityCheck(string storeId)
        {
            List<OrderLineAvailability> orderLineAvailabilities = null;

            BeginWsCall();

            ShowIndicator(true);

            var orderAvailabilityRequest = new OrderAvailabilityRequest()
            {
                CardId = AppData.Device.UserLoggedOnToDevice.Card.Id,
                StoreId = storeId,
                SourceType = SourceType.LSOmni
            };

            int lineNumber = 1;
            foreach (var basketItem in AppData.Basket.Items)
            {
                var variantId = string.Empty;

                if (basketItem.VariantReg != null)
                {
                    variantId = basketItem.VariantReg.Id;
                }

                orderAvailabilityRequest.OrderLineAvailabilityRequests.Add(new OrderLineAvailability()
                {
                    ItemId = basketItem.Item.Id,
                    UomId = basketItem.UnitOfMeasureId,
                    VariantId = variantId,
                    Quantity = basketItem.Quantity,
                    LineNumber = lineNumber++
                });
            }

            try
            {
                orderLineAvailabilities = await service.OrderAvailabilityCheckAsync(orderAvailabilityRequest);
            }
            catch (Exception ex)
            {
                await HandleUIExceptionAsync(ex);
            }

            ShowIndicator(false);

            return orderLineAvailabilities;
        }

        public async Task<bool> ClickCollectOrderCreate(OneList basket, string contactId, string cardId, string storeId, string email)
        {
            bool success = false;
            Order result = null;

            BeginWsCall();

            ShowIndicator(true);

            try
            {
                //success = await service.OrderCreateAsync(service.CreateOrderForCAC(basket, contactId, cardId, storeId, email));
                result = await service.OrderCreateAsync(service.CreateOrderForCAC(basket, contactId, cardId, storeId, email));

                //if (success)
                if(result != null)
                {
                    await basketModel.ClearBasket();
                    ShowToast(Resource.String.CheckoutViewOrderSuccess, ToastLength.Long);
                    success = true;
                }
            }
            catch (Exception ex)
            {
                await HandleUIExceptionAsync(ex);
            }

            ShowIndicator(false);

            return success;
        }

        public List<OneListItem> CreateBasketItems(List<OrderLineAvailability> orderLineAvailabilities)
        {
            var basketItems = new List<OneListItem>();
            var unavailableItems = new List<string>();

            foreach (var orderLineAvailability in orderLineAvailabilities)
            {
                OneListItem basketItem = AppData.Basket.ItemGetByIds(orderLineAvailability.ItemId, orderLineAvailability.VariantId, orderLineAvailability.UomId);
                if (basketItem == null)
                    continue;
                
                if (orderLineAvailability.Quantity > 0 && basketItem != null)
                {
                    var availableBasketItem = new OneListItem()
                    {
                        Id = basketItem.Id,
                        Item = basketItem.Item,
                        Amount = basketItem.Amount,
                        NetAmount = basketItem.NetAmount,
                        NetPrice = basketItem.NetPrice,
                        Price = basketItem.Price,
                        TaxAmount = basketItem.TaxAmount,
                        Quantity = basketItem.Quantity,
                        UnitOfMeasure = basketItem.UnitOfMeasure,
                        VariantReg = basketItem.VariantReg,
                    };

                    if (basketItem.Quantity > orderLineAvailability.Quantity)
                    {
                        unavailableItems.Add("-" + (basketItem.Quantity - orderLineAvailability.Quantity) + " " + basketItem.Item.Description);
                        availableBasketItem.Quantity = orderLineAvailability.Quantity;
                    }

                    basketItems.Add(availableBasketItem);
                }
                else
                {
                    if (basketItem != null)
                        unavailableItems.Add("-" + (basketItem.Quantity) + " " + basketItem.Item.Description);
                }
            }

            return basketItems;
        }

        protected override void CreateService()
        {
			service = new BasketService(new BasketsRepository());
		}
    }
}